﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Вычисление стоимости поездки на дачу.");
            double itog;
            Console.WriteLine("Введите расстояние до дачи (км) - ");
            double km = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите расход бензина (л на 100 км) - ");
            double rashod = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите цену за 1 литр - ");
            double price = Convert.ToDouble(Console.ReadLine());
            itog = (km / 100 * rashod * price)*100;
            Console.WriteLine("Поездка обойдется в " + Math.Floor(itog/100) + " руб. " + Math.Floor(itog%100) + " коп.");
            System.Console.ReadLine();
        }
    }
}